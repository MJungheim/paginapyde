// Database Paths
var microfonoPath = '/sensor/microfono';
var alarmaActivadaPath = '/opciones/alarmaActivada';
var testeoAlarmaPath = '/opciones/testeoAlarma';
var sensibilidadPath = '/opciones/sensibilidad';
// Get a database reference 
const databaseMicrofono = database.ref(microfonoPath);
const databaseAlarmaActivada = database.ref(alarmaActivadaPath);
const databaseTesteoAlarma = database.ref(testeoAlarmaPath);
const databaseSensAlarma = database.ref(sensibilidadPath);
// Checkboxes
const alarmaActivada = document.getElementById("cbox1");
const testeoAlarma = document.getElementById("cbox2");
const sensibilidad = document.getElementById("slider1");

var microfonoReading;
var alarmaReading;
var testeoReading;
var sensibilidadReading;

databaseAlarmaActivada.on('value', (snapshot) => {
  alarmaReading = snapshot.val();
  alarmaActivada.checked = alarmaReading;
  if(alarmaReading == true){
    document.getElementById("reading-bool").innerHTML = "activo";
  }
  if(alarmaReading == false){
    document.getElementById("reading-bool").innerHTML = "inactivo";
  }
}, (errorObject) => {
  console.log('The read failed: ' + errorObject.name);
});
databaseTesteoAlarma.on('value', (snapshot) => {
  testeoReading = snapshot.val();
  testeoAlarma.checked = testeoReading;
  if(testeoReading == true){
    document.getElementById("reading-testeo").innerHTML = "activo";
  }
  if(testeoReading == false){
    document.getElementById("reading-testeo").innerHTML = "inactivo";
  }
}, (errorObject) => {
  console.log('The read failed: ' + errorObject.name);
});

databaseMicrofono.on('value', (snapshot) => {
  microfonoReading = snapshot.val();
  console.log(microfonoReading);
  document.getElementById("reading-int").innerHTML = microfonoReading;
}, (errorObject) => {
  console.log('The read failed: ' + errorObject.name);
});

databaseSensAlarma.on('value', (snapshot) => {
  sensibilidadReading = snapshot.val();
  sensibilidad.value = sensibilidadReading;
  console.log(sensibilidadReading);
  document.getElementById("reading-sensibilidad").innerHTML = sensibilidadReading;
}, (errorObject) => {
  console.log('The read failed: ' + errorObject.name);
});

var database = firebase.database();
var fireaseRef;

alarmaActivada.addEventListener('change', (event) => {
  if (event.currentTarget.checked) {
    firebaseRef = firebase.database().ref().child("/opciones/alarmaActivada");
    firebaseRef.set(true);
  } else {
    var firebaseRef = firebase.database().ref().child("/opciones/alarmaActivada");
    firebaseRef.set(false);
  }
})
testeoAlarma.addEventListener('change', (event) => {
  if (event.currentTarget.checked) {
    firebaseRef = firebase.database().ref().child("/opciones/testeoAlarma");
    firebaseRef.set(true);
  } else {
    firebaseRef = firebase.database().ref().child("/opciones/testeoAlarma");
    firebaseRef.set(false);
  }
})
